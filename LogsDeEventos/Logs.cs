﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;

/// <summary>
/// Programming by Ing. Alejandro Volcán
/// Email: alejandrovolcan@gmail.com
/// Created on 24-02-2021
/// Version 1.0.0
/// </summary>

namespace LogsDeEventos
{
    public class Logs
    {
        #region Internal Log

        public void ErrorLog(object Fuente, Exception error)
        {
            //Name of the directory and file for internal logs
            string rutaCarpeta = ConfigurationManager.AppSettings.Get("CarpetaLogs");
            string ArchivoLog = rutaCarpeta + @"\MueveReina.txt";

            //Registry Variable
            StackTrace stacktrace = new StackTrace();
            
            //Validate if the directory exist
            if (Directory.Exists(rutaCarpeta))
            {
                if (File.Exists(ArchivoLog))
                {
                    StreamWriter sw = File.AppendText(ArchivoLog);
                    sw.WriteLine("--> Error Presented on " + DateTime.Today.ToString("dd-MM-yy ") + DateTime.Now.ToString("HH:mm:ss"));
                    sw.WriteLine(Fuente.GetType().FullName);
                    sw.WriteLine(stacktrace.GetFrame(1).GetMethod().Name + " - " + error);
                    sw.Close();
                }
                else
                {
                    StreamWriter sw = new StreamWriter(ArchivoLog);
                    sw.WriteLine("--> Error Presented on " + DateTime.Today.ToString("dd-MM-yy ") + DateTime.Now.ToString("HH:mm:ss"));
                    sw.WriteLine(Fuente.GetType().FullName);
                    sw.WriteLine(stacktrace.GetFrame(1).GetMethod().Name + " - " + error);
                    sw.Close();
                }
            }
            else
            {
                Directory.CreateDirectory(rutaCarpeta);
                StreamWriter sw = new StreamWriter(ArchivoLog);
                sw.WriteLine("--> Error Presented on " + DateTime.Today.ToString("dd-MM-yy ") + DateTime.Now.ToString("HH:mm:ss"));
                sw.WriteLine(Fuente.GetType().FullName);
                sw.WriteLine(stacktrace.GetFrame(1).GetMethod().Name + " - " + error);
                sw.Close(); ;
            }
        }

        #endregion

        #region Method to write to the event log in Microsoft

        public void LogsWindows(string mensaje, string tipo)
        {
            try
            {
                eventosLogs evento = new eventosLogs
                {
                    Origen = "MueveReina",
                    TipoOrigen = "MueveReina",
                    Evento = "winEventos",
                    Mensaje = mensaje,
                    TipoEntrada = tipo
                };

                //Record the event in the event viewer
                EscribirMensajeLog(evento);

            }
            catch (Exception ex)
            { ErrorLog(this, ex); }
        }

        public void EscribirMensajeLog(eventosLogs evento)
        {
            try
            {
                EventLog miLog = new EventLog(evento.TipoOrigen, ".", evento.Origen);

                //Check if the event log exists
                if (!EventLog.SourceExists(evento.Origen))
                {
                    //If the event log does not exist, we create it
                    EventLog.CreateEventSource(evento.Origen, evento.TipoOrigen);
                }
                else
                {
                    // Retrieves the corresponding event log from the source
                    evento.TipoOrigen = EventLog.LogNameFromSourceName(evento.Origen, ".");
                }

                miLog.Source = evento.Origen;
                miLog.Log = evento.TipoOrigen;

                //Check the type of annotation and record the event
                switch (evento.TipoEntrada)
                {
                    case "1":
                        miLog.WriteEntry(evento.Mensaje, EventLogEntryType.Error);
                        break;
                    case "2":
                        miLog.WriteEntry(evento.Mensaje, EventLogEntryType.FailureAudit);
                        break;
                    case "3":
                        miLog.WriteEntry(evento.Mensaje, EventLogEntryType.Information);
                        break;
                    case "4":
                        miLog.WriteEntry(evento.Mensaje, EventLogEntryType.SuccessAudit);
                        break;
                    case "5":
                        miLog.WriteEntry(evento.Mensaje, EventLogEntryType.Warning);
                        break;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion




        public void testDoc(string error)
        {
            string rutaCarpeta = ConfigurationManager.AppSettings.Get("CarpetaLogs");
            string ArchivoLog = rutaCarpeta + @"\MueveReina.txt";

            if (Directory.Exists(rutaCarpeta))
            {
                if (File.Exists(ArchivoLog))
                {
                    StreamWriter sw = File.AppendText(ArchivoLog);
                    sw.WriteLine("--> Error Presentado el " + DateTime.Today.ToString("dd-MM-yy ") + DateTime.Now.ToString("HH:mm:ss") + " " + error);
                    sw.Close();
                }
                else
                {
                    StreamWriter sw = new StreamWriter(ArchivoLog);
                    sw.WriteLine("--> Error Presentado el " + DateTime.Today.ToString("dd-MM-yy ") + DateTime.Now.ToString("HH:mm:ss") + " " + error);
                    sw.Close();
                }
            }
            else
            {
                Directory.CreateDirectory(rutaCarpeta);
                StreamWriter sw = new StreamWriter(ArchivoLog);
                sw.WriteLine("--> Error Presentado el " + DateTime.Today.ToString("dd-MM-yy ") + DateTime.Now.ToString("HH:mm:ss") + " " + error);
                sw.Close();
            }
        }









    }
}
