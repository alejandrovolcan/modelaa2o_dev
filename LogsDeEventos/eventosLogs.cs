﻿
namespace LogsDeEventos
{
    public class eventosLogs
    {

        string origen;
        /// Name of the application or service that generates the event      
        public string Origen
        {
            get
            { return origen; }
            set
            { origen = value; }
        }

        string tipoOrigen;
        /// Type of event to be noted. Possible options (Application / System / custom name)        
        public string TipoOrigen
        {
            get
            { return tipoOrigen; }
            set
            { tipoOrigen = value; }
        }

        string evento;
        /// Name of the event to be audited        
        public string Evento
        {
            get
            { return evento; }
            set
            { evento = value; }
        }

        string mensaje;
        /// Text of the message to be annotated
        public string Mensaje
        {
            get
            { return mensaje; }
            set
            { mensaje = value; }
        }

        string tipoEntrada;
        /// Type of input for the event. Possible options (1 = Error / 2 = FailureAudit / 3 = Information / 4 = SuccessAudit / 5 = Warning)      
        public string TipoEntrada
        {
            get
            { return tipoEntrada; }
            set
            { tipoEntrada = value; }
        }



    }
}
