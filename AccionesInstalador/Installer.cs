﻿#region Referencias
using System;
using System.Collections;
using System.ComponentModel;
using System.ServiceProcess;
#endregion

/// <summary>
/// Programando por el Ing. Alejandro Volcán
/// Correo: alejandrovolcan@gmail.com
/// Creado el 24-02-2021
/// Version 1.0.0
/// </summary>

namespace AccionesInstalador
{
    [RunInstaller(true)]
    public partial class Installer : System.Configuration.Install.Installer
    {
        ServiceController servicio = new ServiceController("MovimientosReina");

        public Installer()
        {
            InitializeComponent();
        }

        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand)]
        public override void Install(IDictionary stateSaver)
        {
            base.Install(stateSaver);
        }

        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand)]
        public override void Commit(IDictionary savedState)
        {
            var controller = new ServiceController(servicio.ServiceName);
            if ((controller.Status == ServiceControllerStatus.Stopped) ||
            (controller.Status == ServiceControllerStatus.StopPending))
            {
                controller.Start();
                System.Diagnostics.Process.Start(base.Context.Parameters["TARGETDIR"].ToString() + "\\ModeladoA2oDev.exe");
            }

            base.Commit(savedState);
            base.Dispose();                     
        }

        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand)]
        public override void Rollback(IDictionary savedState)
        {
            base.Rollback(savedState);
        }

        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand)]
        public override void Uninstall(IDictionary savedState)
        {
            ServiceController service = new ServiceController(servicio.ServiceName);
            if (service.Status != ServiceControllerStatus.Stopped)
            {
                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, new TimeSpan(0, 1, 0));
            }
            service.Dispose();

            base.Uninstall(savedState);
        }
    }
}