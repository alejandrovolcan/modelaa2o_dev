﻿#region Referencias

using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using LogsDeEventos;

#endregion

/// <summary>
/// Programming by Ing. Alejandro Volcán
/// Email: alejandrovolcan@gmail.com
/// Created on 24-02-2021
/// Version 1.0.0
/// </summary>

namespace ReinaMueve
{
    partial class MovimientoReina : ServiceBase
    {
        #region Variables or classes

        readonly Logs logs = new Logs();
        
        #endregion

        public MovimientoReina()
        {
            InitializeComponent();
            this.CanStop = true;
            this.CanPauseAndContinue = true;
            this.AutoLog = true;
        }

        //Service Start
        protected override void OnStart(string[] args)
        {
            try
            {
                var RutaInterfaz = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "ModeladoA2oDev.exe");
                int cont = 0;

                foreach (Process procesos in Process.GetProcessesByName("ModeladoA2oDev"))
                { cont++; }

                if (cont == 0)
                {
                    ProcessStartInfo Intee = new ProcessStartInfo
                    {
                        FileName = RutaInterfaz,
                        WindowStyle = ProcessWindowStyle.Maximized,
                        UseShellExecute = true,
                        CreateNoWindow = false
                    };                    
                    Process.Start(Intee);
                }
            }
            catch(Exception ex)
            {
                logs.LogsWindows(ex.Message, "1");
            }
        }

        //Service Stop
        protected override void OnStop()
        {
            try
            {
                foreach (Process procesos in Process.GetProcessesByName("ModeladoA2oDev"))
                {
                    procesos.Kill();
                    procesos.WaitForExit();
                    logs.LogsWindows("The interface was stopped", "3");                     
                }                
            }
            catch(Exception ex)
            {
                logs.LogsWindows(ex.Message, "1");
            }
        }
    }
}