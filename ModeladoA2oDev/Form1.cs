﻿#region Referencias
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using LogsDeEventos;
#endregion

/// <summary>
/// Programming by Ing. Alejandro Volcán
/// Email: alejandrovolcan@gmail.com
/// Created on 24-02-2021
/// Version 1.0.0
/// </summary>

namespace ModeladoA2oDev
{
    public partial class Principal : Form
    {
        #region Classes and General variables

        readonly Logs logs = new Logs();        
        private List<int> ElementosX = new List<int>();
        private List<int> ElementosY = new List<int>();

        #endregion

        #region Application Load

        public Principal()
        { InitializeComponent(); }

        private void Principal_Load(object sender, EventArgs e)
        {}

        #endregion
        
        //Main Function
        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(TxtInput.Text))
                {
                    string[] Lineas = TxtInput.Lines;

                    string[] nk;
                    int n = 0;
                    int k = 0;

                    string[] r_qC_q;
                    int rq = 0;
                    int cq = 0;

                    string[] Obst;
                    int[] ObstPosX;
                    int[] ObstPosY;

                    List<int> ElementosX1 = new List<int>();
                    List<int> ElementosY1 = new List<int>();

                    nk = Lineas[0].Split(' ');
                    n = Convert.ToInt32(nk[0]);
                    k = Convert.ToInt32(nk[1]);

                    r_qC_q = Lineas[1].Split(' ');
                    rq = Convert.ToInt32(r_qC_q[0]);
                    cq = Convert.ToInt32(r_qC_q[1]);
                    string resultado;
                    AtaqueReina a = new AtaqueReina();

                    if (k == 0)
                    {
                        ElementosX.Add(0);
                        ElementosY.Add(0);
                        ObstPosX = ElementosX1.ToArray();
                        ObstPosY = ElementosY1.ToArray();
                        resultado = Convert.ToString(a.CantidadMovimientos(n, k, rq, cq, ObstPosX, ObstPosY));
                        TxtOuput.Text = "The available movements are: " + resultado;
                    }
                    else
                    {
                        for (int mov = 2; mov < Lineas.Length; mov++)
                        {
                            Obst = Lineas[mov].Split(' ');
                            int valorx = Convert.ToInt32(Obst[0]);
                            int valory = Convert.ToInt32(Obst[1]);
                            ElementosX1.Add(valorx);
                            ElementosY1.Add(valory);                            
                        }
                        ObstPosX = ElementosX1.ToArray();
                        ObstPosY = ElementosY1.ToArray();
                        resultado = Convert.ToString(a.CantidadMovimientos(n, k, rq, cq, ObstPosX, ObstPosY));
                        TxtOuput.Text = "The available movements are: " + resultado;
                    }
                }
                else
                {
                    TxtOuput.Text = "";
                }
            }
            catch(Exception ex)
            {
                logs.ErrorLog(this, ex);
            }
        }




        #region Validations

        private void TxtInput_KeyPress(object sender, KeyPressEventArgs e)
        { SoloNumerosOEspacios(e); }

        //Minimize button to taskbar
        private void Minimizar_Click(object sender, EventArgs e)
        {
            MinimizarAplicacion();
        }

        //Maximize the application
        private void IconTray_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            IconTray.Visible = false;
            WindowState = FormWindowState.Normal;
        }

        //Control when the app is brought to the fore or minimized
        private void Principal_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            { MinimizarAplicacion(); }
        }

        //It does not allow the closing of the application, it is minimized to the taskbar
        private void Principal_FormClosing(object sender, FormClosingEventArgs e)
        {
            switch (e.CloseReason)
            {
                case CloseReason.UserClosing:
                    e.Cancel = true;
                    MinimizarAplicacion();
                    break;
            }
        }

        //Minimize to the taskbar
        private void MinimizarAplicacion()
        {
            try
            {
                WindowState = FormWindowState.Minimized;
                ShowInTaskbar = false;
                IconTray.Visible = true;
                IconTray.BalloonTipText = "Application Running ";
                IconTray.BalloonTipTitle = "Alejandro Volcan";
                IconTray.ShowBalloonTip(1000);
            }
            catch (Exception ex)
            {
                logs.ErrorLog(this, ex);
            }
        }

        //Allow entry of numbers only or blank space
        private void SoloNumerosOEspacios(KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            { e.Handled = false; }
            else
            if (Char.IsControl(e.KeyChar))
            { e.Handled = false; }
            else
            if (Char.IsSeparator(e.KeyChar))
            { e.Handled = false; }
            else
            { e.Handled = true; }
        }
        
        public void Maximi()
        {
            Principal p = new Principal();
            p.Show();
        }

        #endregion      
    }
}