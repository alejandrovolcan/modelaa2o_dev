﻿using System;
using LogsDeEventos;

/// <summary>
/// Programming by Ing. Alejandro Volcán
/// Email: alejandrovolcan@gmail.com
/// Created on 24-02-2021
/// Version 1.0.0
/// </summary>

namespace ModeladoA2oDev
{
    class AtaqueReina
    {
        readonly Logs logs = new Logs();


        //Calculating the Queen's Moves

        public int CantidadMovimientos(int n, int k, int x, int y, int[] obstPosx, int[] obstPosy)
        {
            try
            {
                int d11, d12, d21, d22, r1, r2, c1, c2;

                d11 = Math.Min(x - 1, y - 1);
                d12 = Math.Min(n - x, n - y);
                d21 = Math.Min(n - x, y - 1);
                d22 = Math.Min(x - 1, n - y);

                r1 = y - 1;
                r2 = n - y;
                c1 = x - 1;
                c2 = n - x;

                for (int i = 0; i < k; i++)
                {
                    if (x > obstPosx[i] && y > obstPosy[i] &&
                            x - obstPosx[i] == y - obstPosy[i])
                        d11 = Math.Min(d11, x - obstPosx[i] - 1);

                    if (obstPosx[i] > x && obstPosy[i] > y &&
                            obstPosx[i] - x == obstPosy[i] - y)
                        d12 = Math.Min(d12, obstPosx[i] - x - 1);

                    if (obstPosx[i] > x && y > obstPosy[i] &&
                            obstPosx[i] - x == y - obstPosy[i])
                        d21 = Math.Min(d21, obstPosx[i] - x - 1);

                    if (x > obstPosx[i] && obstPosy[i] > y &&
                                x - obstPosx[i] == obstPosy[i] - y)
                        d22 = Math.Min(d22, x - obstPosx[i] - 1);

                    if (x == obstPosx[i] && obstPosy[i] < y)
                        r1 = Math.Min(r1, y - obstPosy[i] - 1);

                    if (x == obstPosx[i] && obstPosy[i] > y)
                        r2 = Math.Min(r2, obstPosy[i] - y - 1);

                    if (y == obstPosy[i] && obstPosx[i] < x)
                        c1 = Math.Min(c1, x - obstPosx[i] - 1);

                    if (y == obstPosy[i] && obstPosx[i] > x)
                        c2 = Math.Min(c2, obstPosx[i] - x - 1);
                }

                return d11 + d12 + d21 + d22 + r1 + r2 + c1 + c2;
            }
            catch(Exception ex)
            {
                logs.ErrorLog(this, ex);
                throw;
            }
        }
    }
}